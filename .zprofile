#!/usr/bin/zsh
# Adds ~/.local/bin to $PATH
export PATH="$PATH:${$(find ~/.local/bin -type d -printf %p:)%%:}"
unsetopt PROMPT_SP
# Defaults
export EDITOR="nvim"
export TERMINAL="st"
export BROWSER="brave"
# Dotfile cleanup
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"

# Start graphical server on user's current tty if not already running.
[ "$(tty)" = "/dev/tty1" ] && ! pidof -s Xorg >/dev/null 2>&1 && exec startx "$XINITRC"
