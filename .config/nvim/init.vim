" Plugins
	packadd! dracula
	syntax enable
	colorscheme dracula

" Basics
	set number relativenumber
	set encoding=utf-8

" Autocomplete
	set wildmode=longest,list,full

" Disable autocomment
	autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Proper splits
	set splitbelow splitright

" Split Nav
	map <C-h> <C-w>h
	map <C-j> <C-w>j
	map <C-k> <C-w>k
	map <C-l> <C-w>l

"Compiles .tex files on save
"	autocmd BufWritePost *.tex !latexmk -pdflua --quiet *.tex %

" Deletes trailing white space
	autocmd BufWritePre * %s/\s\+$//e
